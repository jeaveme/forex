# How to run the app
* Create a Python 3 virtualenv with:
```
mkvirtualenv --python=/usr/bin/python3 forex
```
* Run the make script within the app directory:
```
make run
```

# Style guides
* PEP8 for python code
* Google Style Guide for JS code
