from django.conf.urls import url
from django.contrib import admin

from trades.rest_api import TradeListCreateApiView, GetRateApiView
from trades.views import ForExTradesView, ListTradesAngularTemplateView, NewTradeAngularTemplateView

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    # RESTful API
    url(r'^api/trades/$',
        TradeListCreateApiView.as_view(),
        name='TradeListCreateApiView'),
    url(r'^api/rates/$',
        GetRateApiView.as_view(),
        name='GetRateApiView'),

    url(r'^angular_templates/list_trades.html$',
        ListTradesAngularTemplateView.as_view(),
        name='ListTradesAngularTemplateView'),
    url(r'^angular_templates/new_trade.html$',
        NewTradeAngularTemplateView.as_view(),
        name='NewTradeAngularTemplateView'),

    url(r'^',
        ForExTradesView.as_view(),
        name='ForExTradesView'),
]
