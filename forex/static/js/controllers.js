var ctrlsModule = angular.module('ForexApp.controllers', []);
ctrlsModule.controller('ListTradesController', ['$scope', 'Trade',
function($scope, Trade) {
  Trade.query(function(data) {
    $scope.trades = data;
  });
}]);
ctrlsModule.controller('NewTradeController',
['$scope', '$http', '$window', '$location', 'Trade',
function($scope, $http, $window, $location, Trade) {
  // We expose the available currency options
  $scope.currencies = currenciesJson;

  // Functions
  $scope.retrieveRate = function() {
    if ($scope.sellCurrency && $scope.buyCurrency) {
      if ($scope.sellCurrency === $scope.buyCurrency) {
        $window.alert('Sell and buy currencies can\'t be the same');
        $scope.rate = $scope.buyCurrency = null;
        return;
      }

      var getParams = {
        'sell_currency': $scope.sellCurrency,
        'buy_currency': $scope.buyCurrency
      };
      $http.get(urlGetRateEndpoint, {params: getParams})
        .then(function(response) {
          if (response.data.error) {
            $window.alert(response.data.error);
            $scope.rate = $scope.buyCurrency = null;
          } else {
            $scope.rate = response.data.rate;
          }
        }, function(response) {
          $scope.rate = $scope.buyCurrency = null;
        });
    } else {
      $scope.rate = null;
    }
  };
  $scope.createTrade = function() {
    var newTrade = new Trade({
      'sell_currency': $scope.sellCurrency,
      'sell_amount': $scope.sellAmount,
      'buy_currency': $scope.buyCurrency,
      'rate': $scope.rate
    });
    newTrade.$save(function() {
      $location.path('/');
    });
  };

  // Each time rate or sell amount changes, buy amount is recalculated
  $scope.$watch('rate', function() { calculateBuyAmount($scope); });
  $scope.$watch('sellAmount', function() { calculateBuyAmount($scope); });
}]);

function calculateBuyAmount($scope) {
  if ($scope.rate && $scope.sellAmount) {
    $scope.buyAmount = ($scope.rate * $scope.sellAmount).toFixed(2);
  } else {
    $scope.buyAmount = null;
  }
}
