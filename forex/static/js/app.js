var app = angular.module('ForexApp', ['ngRoute', 'ngResource',
  'ForexApp.controllers']);

app.config(['$routeProvider', '$locationProvider',
function($routeProvider, $locationProvider) {
  $routeProvider.when('/', {
    templateUrl: templateListTrades,
    controller: 'ListTradesController'
  })
  .when('/new_trade', {
    templateUrl: templateNewTrade,
    controller: 'NewTradeController'
  })
  .otherwise({redirectTo: '/'});
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
}]);
app.config(['$resourceProvider', function($resourceProvider) {
  $resourceProvider.defaults.stripTrailingSlashes = false;
}]);
app.factory('Trade', ['$resource', function($resource) {
  return $resource(urlTradesEndpoint + ':id');
}]);
