from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.crypto import get_random_string


class Currency(models.Model):
    code = models.CharField(max_length=3, unique=True,
                            verbose_name=_('Code'))

    class Meta:
        verbose_name = _('Currency')
        verbose_name_plural = _('Currencies')
        ordering = ('code',)

    def __str__(self):
        return self.code


class Trade(models.Model):
    id = models.CharField(max_length=9, primary_key=True)
    sell_currency = models.ForeignKey(Currency,
                                      related_name='selling_trades',
                                      verbose_name=_('Sell CCY'))
    sell_amount = models.DecimalField(max_digits=9, decimal_places=2,
                                      verbose_name=_('Sell amount'))
    buy_currency = models.ForeignKey(Currency,
                                     related_name='buying_trades',
                                     verbose_name=_('Buy CCY'))
    buy_amount = models.DecimalField(max_digits=9, decimal_places=2,
                                     verbose_name=_('Buy amount'))
    rate = models.DecimalField(max_digits=7, decimal_places=4,
                               verbose_name=_('Rate'))
    date_booked = models.DateTimeField(verbose_name=_('Date booked'),
                                       auto_now_add=True)

    class Meta:
        verbose_name = _('Trade')
        ordering = ('-date_booked',)

    def __str__(self):
        return self.id

    def save(self, *args, **kwargs):
        # TODO The specification doesn't make it clear if ids should be randomized. This should be clarified
        while not self.id:
            random_part = get_random_string(7, allowed_chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
            random_id = 'TR%s' % random_part
            if not type(self).objects.filter(pk=random_id).count():
                self.id = random_id

        super().save(*args, **kwargs)
