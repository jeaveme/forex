from rest_framework.generics import ListCreateAPIView, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from django.utils.translation import ugettext as _

from .utils import get_forex_rate
from .models import Trade, Currency
from .serializers import NewTradeSerializer, TradeSerializer, GetRateSerializer


class TradeListCreateApiView(ListCreateAPIView):
    queryset = Trade.objects.all()

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return NewTradeSerializer
        return TradeSerializer

    def perform_create(self, serializer):
        buy_amount = serializer.validated_data['sell_amount'] * serializer.validated_data['rate']
        serializer.save(buy_amount=buy_amount)


class GetRateApiView(APIView):
    """
    The external service fixer.io is wrapped by this view due to:
    - Abstract the endpoint from a concrete provider to be able to change it more easily
    - Have the chance to cache queried results to reduce our dependency on that service (TODO)
    """
    serializer_class = GetRateSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        sell_currency = get_object_or_404(Currency, pk=serializer.validated_data['sell_currency'])
        buy_currency = get_object_or_404(Currency, pk=serializer.validated_data['buy_currency'])

        response_data = get_forex_rate(sell_currency.code, buy_currency.code)
        if response_data is None:
            response_data = {'error': _('Communication error with foreign exchange rate provider')}

        return Response(response_data)
