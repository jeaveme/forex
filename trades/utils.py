import requests

from django.conf import settings


def get_forex_rate(sell_currency_code, buy_currency_code):
    req = requests.get(settings.URL_FOREX_RATE_PROVIDER,
                       params={'base': sell_currency_code,
                               'symbols': buy_currency_code})
    retrieved_rate = req.json()
    try:
        date = retrieved_rate['date']
        rate = round(retrieved_rate['rates'][buy_currency_code], 4)
    except KeyError:
        rate = None
    else:
        rate = {'sell_currency_code': sell_currency_code,
                'buy_currency_code': buy_currency_code,
                'date': date,
                'rate': rate}
    return rate
