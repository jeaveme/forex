from django.contrib import admin

from .models import Currency, Trade


class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('code',)


class TradeAdmin(admin.ModelAdmin):
    list_display = ('id', 'sell_currency', 'sell_amount', 'buy_currency', 'buy_amount',
                    'rate', 'date_booked')


admin.site.register(Currency, CurrencyAdmin)
admin.site.register(Trade, TradeAdmin)
