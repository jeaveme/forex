import json

from django.views.generic import TemplateView

from trades.models import Currency


class ForExTradesView(TemplateView):
    template_name = 'trades/main.html'

    def get_context_data(self, **kwargs):
        context = super(ForExTradesView, self).get_context_data(**kwargs)
        qs_currencies = Currency.objects.all()
        currencies = [{'id': currency.id, 'code': currency.code}
                      for currency in qs_currencies]
        context['currencies_json'] = json.dumps(currencies)
        return context


class ListTradesAngularTemplateView(TemplateView):
    template_name = 'trades/list_trades.html'


class NewTradeAngularTemplateView(TemplateView):
    template_name = 'trades/new_trade.html'
