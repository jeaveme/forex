from rest_framework import serializers
from django.utils.translation import ugettext as _

from .models import Trade
from .utils import get_forex_rate


class TradeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trade
        fields = ('sell_currency', 'sell_amount', 'buy_currency', 'buy_amount',
                  'rate', 'date_booked',)

    def to_representation(self, obj):
        ret = super().to_representation(obj)
        ret.update({'sell_currency': obj.sell_currency.code,
                    'buy_currency': obj.buy_currency.code})
        return ret


class NewTradeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Trade
        fields = ('sell_currency', 'sell_amount', 'buy_currency', 'rate',)

    def validate(self, data):
        sell_currency = data['sell_currency']
        buy_currency = data['buy_currency']
        rate = float(data['rate'])

        # Check that supplied rate wasn't tampered by the client or is outdated
        response_data = get_forex_rate(sell_currency.code, buy_currency.code)
        if response_data is None:
            raise serializers.ValidationError({'rate': _('Communication error with rate provider. '
                                                         'Please, try again later.')})
        elif response_data['rate'] != rate:
            raise serializers.ValidationError({'rate': _('Rate outdated. Please, try again.')})
        return data


class GetRateSerializer(serializers.Serializer):
    sell_currency = serializers.IntegerField(min_value=1)
    buy_currency = serializers.IntegerField(min_value=1)
